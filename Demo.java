import javax.swing.*;        
import java.awt.Font;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

 
public class Demo implements ActionListener {
    JLabel label;

 
    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }

        
    private static void createAndShowGUI() {
        JFrame frame = new JFrame("Hello");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 
        label = new JLabel("Hello");
        frame.getContentPane().add(label);

                label.setFont( new Font( "Courier", Font.BOLD, 18 ) );
                label.setBackground( Color.BLUE );
                label.setOpaque( true );
                label.setForeground( Color.YELLOW );

                JButton button = new JButton( "Click here" );
        frame.getContentPane( ).add( button );
                button.addActionListener( this );

                frame.getContentPane( ).setLayout( new FlowLayout( ) );

        frame.pack();
        frame.setVisible(true);
    }

    public void actionPerformed( ActionEvent event ) {
        label.setText( "You clicked me" );
    }

}