import java.io.*;
import java.util.*;
import java.util.Random;

class ticTac{
	public static void main(String[] args)
	{

	String board[];
	board = new String[9];
	int i = 0;
	boolean gameOver = false;
	
	
	while(i < 9)
	{
		board[i] = "-";
		i = i + 1;
	}
	i = 0;
	
	while(gameOver == false)
	{
	printBoard(board);
	pMove(board);
	cMove(board);
	gameOver = checkGame(board);
	}
	
	}

	public static boolean checkGame(String[]board)
	{
		if(board[0] == "X" && board[1] == "X" && board[2] == "X")
		{
		return(true);
		}
		else
		{
		return(false);
		}
	}
	
	public static void cMove(String[]board)
	{
		int i = 0;
        Random generator = new Random();
        i = generator.nextInt(8);
		
		if(board[i] != "-")
		{
			cMove(board);
		}
		else
		{
		board[i] = "O";
		}
	}
	
	public static void pMove(String[]board)
	{
		int i = 0;
		Scanner input = new Scanner(System.in);	
		System.out.println("where would you like to move?");
		i = input.nextInt();
		
		if(i > 8 || (board[i] != "-"))
		{
			System.out.println("Try again 0 - 8");
			printBoard(board);
			pMove(board);
		}
		else
		{
		board[i] = "X";
		}
	}
	
	public static void printBoard(String[]board)
	{
		System.out.println("");
		System.out.println( board[0] + " | " + board[1] + " | " + board[2]);
		System.out.println( board[3] + " | " + board[4] + " | " + board[5]);
		System.out.println( board[6] + " | " + board[7] + " | " + board[8]);
		System.out.println("");
	}
	
}