import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Random;
import java.util.List;

public class C108 {

	public static void main( String [ ] args ) {
		SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new Gui();
            }
        });
	}
}

class Gui {	
	JFrame frame;
	Container pane;

	public Gui( ) {
		frame = new JFrame( "Staff" );
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize( new Dimension( 400, 400 ) );
		pane = frame.getContentPane( );
		Person p = new Person( );
		joe j = new joe();
		pane.add(j);
		pane.add( p );
        frame.setVisible(true);
	}
}

class Person extends JPanel {
	public Person( ) {
		this.setLocation( new Point( 10, 20 ) );
		this.setSize( new Dimension( 20, 30 ) );
		worker.execute( );
	}

	SwingWorker worker = new SwingWorker <Integer, Integer> ( ) {
		public Integer doInBackground( ) {
			while( !worker.isCancelled( ) ) {
				try {
					Thread.sleep( 1000 );
				} catch( InterruptedException ex ) { }
				publish( 1 );
			}
			return 1;
		}
		

		protected void process(List<Integer> chunks ) {
			Point p = new Point( );
			p.setLocation( (getLocation().getX() + 20) % 300, 20.0 );
			setLocation( p );
			repaint( );
//			worker.cancel( true );
		}
	};

	public void paintComponent (Graphics g) {
		super.paintComponent( g );
		Graphics2D g2 = (Graphics2D) g;
		g2.fillOval( 5, 0, 10, 10 );
		g2.drawLine( 10, 10, 10, 20 );
		g2.drawLine( 10, 20, 5, 25 );
		g2.drawLine( 10, 20, 15, 25 );
		g2.drawLine( 5, 15, 15, 15 );
	}

}