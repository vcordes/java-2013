import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

public class awesomeTest
{
	public static void main(String[] args){

	   javax.swing.SwingUtilities.invokeLater(new Runnable() {
	      public void run(){
		  test UI = new test();
		  UI.setVisible(true);
	      }
  	   });
       }
}

class test extends JFrame {

	public test(){
		testUI();
	}

public void testUI() {

	JMenuBar menuBar = new JMenuBar();

	JMenu file = new JMenu("File");
	file.setMnemonic(KeyEvent.VK_F);

	JMenuItem aMenuItem = new JMenuItem("Exit");
	aMenuItem.setMnemonic(KeyEvent.VK_E);
	aMenuItem.setToolTipText("Exit application");
	aMenuItem.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent event) {
			System.exit(0);
		}
	});

	JMenuItem bMenuItem = new JMenuItem("Open");
	bMenuItem.setMnemonic(KeyEvent.VK_G);
	bMenuItem.setToolTipText("Open a new frame");
	bMenuItem.addActionListener(new ActionListener(){
		 public void actionPerformed(ActionEvent event2) {
			testUI();
		}
	});

	file.add(bMenuItem);
	file.add(aMenuItem);

	menuBar.add(file);

	setJMenuBar(menuBar);

	setTitle("awesomeTest");
	setSize(300, 300);
	setLocationRelativeTo(null);
	setDefaultCloseOperation(EXIT_ON_CLOSE);

	}
}

