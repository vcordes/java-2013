import javax.swing.*;
import java.awt.Font;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.Random;
import java.lang.Integer;


public class numberGuess2
{
    public static void main(String[] args)
	{
        javax.swing.SwingUtilities.invokeLater(new Runnable()
		{
            public void run()
			{
                Guess guess = new Guess();
            }
        });
    }
}

class Guess implements ActionListener{

    JLabel prompt;
    JTextField guess;
    Integer number;
    public Guess()
			{
            JFrame frame = new JFrame("NumberGuess");
            frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
            frame.setSize( 300, 300 );
            frame.setVisible( true );

            Random generator = new Random();
            number = generator.nextInt(10);

            prompt = new JLabel("Guess my Number", JLabel.CENTER);
            frame.getContentPane().add( prompt );
            prompt.setFont( new Font( "Ariel", Font.BOLD, 20) );
            prompt.setForeground( Color.RED);

            guess = new JTextField( 10 );
            frame.getContentPane().add( guess );

            JButton button = new JButton("Check your answer?");
            frame.getContentPane().add(button);
            button.addActionListener(this);

            frame.getContentPane( ).setLayout( new FlowLayout());
            }

        public void actionPerformed( ActionEvent event ) {
	        if (Integer.parseInt(guess.getText()) > (number))
			prompt.setText( "You guessed too high" );
		else if (Integer.parseInt(guess.getText()) < (number))
			prompt.setText( "You guessed too low" );
		else if (Integer.parseInt(guess.getText()) == (number)) 
                	prompt.setText( " Correct " );
            
        }

}
