import java.io.*;
import java.util.*;

class Josephus{
	public static void main(String[] args){
	
	
	Scanner num = new Scanner(System.in);
	int number = 0;
	int[] prisoners;
	int exeStep = 0;	
	int survivors = 0;
	boolean check = false;
	
	//get number of prisoners and set the array size
	System.out.println("Enter the number of prisoners");
	number = num.nextInt();
	prisoners = new int[number];		
	setArray(prisoners, number);	
	
	//get the executioners step
	System.out.println("Enter the executioners step");
	exeStep = num.nextInt();
	
	//get the number of survivors
	System.out.println("Enter the number of survivors");
	survivors = num.nextInt();
	System.out.println("");
	
	//execute prisoners
	execute(prisoners, exeStep, number);

	//print the survivors
	printSurv(prisoners, number);


		
	}

//methods	
	public static void printSurv(int[]prisoners, int number)
	{
		int i = 0;
		
		while(i < number)
		{
			if(prisoners[i] != -1)
			{
				System.out.println(prisoners[i]);
			}
			i = i + 1;
		}
	}
	
	public static boolean checkSurv(int[]prisoners, int survivors, int number)
	{
		int i = 0;
		int check = 0;
		
		while(i < number -1)
		{
			if(prisoners[i] != -1)
			{
				check = check + 1;
			}
			i = i + 1;
		}
		
		return(false);
	}
		
	public static void execute(int[]prisoners, int exeStep, int number)
	{
	
		int i = 0;
		int k = 1;
		
		while(i < number - 1)
		{

			if(k == exeStep && (prisoners[i] != -1))
			{
					prisoners[i] = -1;
					k = 1;
					i = i + 1;
			}
			else if (k == exeStep && (prisoners[i] == -1))
			{
				i = i + 1;
			}
			else
			{
				k = k + 1;
				i = i + 1;
			}
		}
	
	}
	
	public static void setArray(int[]prisoners, int number)
	{
	int i = 0;
	
	while(i <= number - 1)
	{
		prisoners[i] = i;
		i = i + 1;
	}

	}	
}