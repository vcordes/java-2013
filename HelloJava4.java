/*another example from the book adds blinking text to the previous code
3 purposes to typing this code
	1) practice java syntax
	2) ensure to read code line by line to further understanding
	3) get used to seeing various complier errors due to typos
		*Bonus on this one I missed the line to new up the HelloComponent4 class.
		 This resulted in a clean compile and nothing happening
		 Thanks to lecture I was able to quickly deduce what the problem was
*/

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class HelloJava4
{
	public static void main( String[] args )
		{
		JFrame frame = new JFrame( "HelloJava4" );
		frame.getContentPane().add( new HelloComponent4( "Hello Java!") );
		frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		frame.setSize( 300, 300 );
		frame.setVisible( true );
		}
}

class HelloComponent4 extends JComponent
	implements MouseMotionListener, ActionListener, Runnable
{
	String theMessage;
	int messageX = 112, messageY = 95;
	
	JButton theButton;
	
	int colorIndex;
	static Color[] someColors =
	{
		Color.black, Color.red, Color.green, Color.blue, Color.magenta 
	};
	
	boolean blinkState;
	
	public HelloComponent4( String message )
	{
		theMessage = message;
		theButton = new JButton( "Change Color" );
		setLayout( new FlowLayout() );
		add( theButton );
		theButton.addActionListener( this );
		addMouseMotionListener( this );
		Thread t = new Thread( this );
		t.start();
	}
	
	public void paintComponent( Graphics g )
	{
		g.setColor(blinkState ? getBackground() : currentColor( ));
		g.drawString(theMessage, messageX, messageY);
	}
	
	public void mouseDragged(MouseEvent e)
	{
		messageX = e.getX();
		messageY = e.getY();
		repaint();
	}
	
	public void mouseMoved(MouseEvent e) {}
	
	public void actionPerformed( ActionEvent e )
	{
		if ( e.getSource() == theButton )
			changeColor();
	}
	
	synchronized private void changeColor()
	{
		if (++colorIndex == someColors.length)
			colorIndex = 0;
		setForeground( currentColor() );
		repaint();
	}
	
	synchronized private Color currentColor( )
	{
		return someColors[colorIndex];
		}
		
		public void run()
		{
			try
			{
				while(true)
				{
					blinkState = !blinkState;
					repaint();
					Thread.sleep(300);
				}
			}
			catch (InterruptedException ie) {}
		}
	}
				