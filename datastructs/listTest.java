import java.io.*;
import java.util.*;

class listTest {
    public static void main(String[] args) {
	
	    List myList = new List();
		Node tmp = new Node();
		
		Scanner input = new Scanner(System.in);
		int selection = 0;
		String loop = "n";
		int in = 0;
		
		while (loop != "y"){
			
			System.out.println("");
			System.out.println("Enter 1 to quit");
			System.out.println("Enter 2 to build a list");
			System.out.println("Enter 3 for a forward display");
			System.out.println("Enter 4 for a backward display");
			System.out.println("Enter 5 to pull a node");
			
			
			selection = input.nextInt();
		
			switch (selection) {
				case 1: loop = "y";
						break;
				case 2: myList.Build();
						break;
				case 3: myList.Displayf();
						break;
				case 4: myList.Displayb();
						break;
				case 5: System.out.println("What node would you like to pull");
						in = input.nextInt();
						tmp = myList.Seek(in);
						myList.Pull(tmp);
						break;
				default: System.out.println("Not a valid option");
						break;
			}
		}
		
		
	}
}