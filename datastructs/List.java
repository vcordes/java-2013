import java.io.*;
import java.util.*;

class List{

		private Node start;
		private Node end;

		public List(){
			start = null;
			end = null;
		}

		public boolean Empty() {
			return start == null;
		}

		public Node Seek(int val){
			Node tmp = start;
			int i = 0;

			if (val == 0){
				return tmp;
			}
			else{
				while(i < val){
					tmp = tmp.next;
					i = i + 1;
					}
			}
			return tmp;
		}

		public void Append(int val){
			Node tmp = new Node(val);

			if(this.start == null){
				start = tmp;
				end = tmp;
			}
			else{
			end.next = tmp;
			tmp.prev = end;
			end = tmp;
			}
		}

		public void Insert(int val){
			Node tmp = new Node(val);
			start.prev = tmp;
			tmp.next = start;
			start = tmp;
		}

		public void Displayf(){
			Node tmp = start;
			System.out.print("List: ");

			while(tmp != null){
				tmp.printValue();
				tmp = tmp.next;
			}
			System.out.println("");
		}

		public void Displayb(){
			Node tmp = end;
			System.out.print("List in reverse: ");

			while(tmp != null){
				tmp.printValue();
				tmp = tmp.prev;
			}
			System.out.println("");
		}

		public void Build(){
			int val = 0;
			Scanner input = new Scanner(System.in);

			System.out.println("Enter a value for your Node (-1 to quit) ");
			val = input.nextInt();

			while(val != -1){
				this.Append(val);
				System.out.println("Enter a value for the next node (-1 to quit) ");
				val = input.nextInt();
			}

		}

		public Node Pull(Node tmp){

			if (tmp == start && tmp == end)
			{
				start = null;
				end = null;
				tmp.prev = null;
				tmp.next = null;
			}
			else if(tmp == start)
			{
				start = tmp.next;
				start.prev = null;
			}
			else if(tmp == end){
				end = tmp.prev;
				end.next = null;
			}
			else{
			tmp.prev.next = tmp.next;
			tmp.next.prev = tmp.prev;
			}

			return tmp;
		}

		public Node getStart(){
			return start;
		}

		public Node getEnd(){
			return end;
		}


		public void Load(int value){

			int i = 1;
			while (i <= value)
			{
				this.Append(i);
				i = i + 1;
			}
		}
}
