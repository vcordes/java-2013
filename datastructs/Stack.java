import java.io.*;
import java.util.*;

class Stack{

		public Node top;
		public List data;
		public int size;
		
		Stack(){
			top = null;
			data = null;
			size = 0;
		}
		
		Stack(int val){
			top = null;
			data = null;
			size = val;
		}

		public void Push(Node tmp){
			
			if (this.data == null){
				Stack myStack = new Stack();
				List staList = new List();
				this.data = staList;
				this.data.Append(1);
				this.top = this.data.getEnd();
			}
			else{
				this.data.Append(2);
				this.top = this.data.getEnd();
			}
			
		}
			
		public void Pop(){
			
			if(this.data == null){
				System.out.println("Nothing to pop");
			}
			else{
				this.data.Pull(this.data.getEnd());
				this.data.Displayf();
			}
			
		}
		
		public void Peek(){

			if(this.data == null){
				System.out.println("Nothing to see here");
			}
			else{
				Node tmp = this.data.getEnd();
				this.Pop();
				tmp.printValue();
				this.Push(tmp);
			}
		}

}
			
			
