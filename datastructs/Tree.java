import java.io.*;
import java.util.*;

class Tree{

		public treeNode root;
		
		Tree(){
			root = null;
		}
		
		public void Build(){
			int val = 0;
			Scanner input = new Scanner(System.in);
			
			
			System.out.println("Enter a value for your Node (-1 to quit) ");
			val = input.nextInt();
			
			if (this.root == null){
				treeNode newNode = new treeNode(val);
				Tree myTree = new Tree();
				this.root = newNode;
			}


			System.out.println("Enter a value for your Node (-1 to quit) ");
			val = input.nextInt();			
						
			while(val != -1){
				
				treeNode newNode = new treeNode(val);
				treeNode tmp = this.root;
				
				
				while (tmp != newNode){

					if ((val <= tmp.getValue()) && (tmp.left != null)){
						tmp = tmp.left;
					}
					else if ((val > tmp.getValue()) && (tmp.right != null)){
						tmp = tmp.right;
					}
					else if ( val <= tmp.getValue()){
						tmp.left = newNode;
						tmp = newNode;
					}
					else{
						tmp.right = newNode;
						tmp = newNode;
					}
				}
				
				System.out.println("Enter a value for your new node (-1 to quit)");
				val = input.nextInt();
			}
		}

		public void Test(){
			treeNode tmp1 = new treeNode(8);
			treeNode tmp2 = new treeNode(9);
			treeNode tmp3 = new treeNode(7);
			Tree newTree = new Tree();
			
			this.root = tmp1;
			root.left = tmp3;
			root.right = tmp2;
			
			
			System.out.println(root.getValue());
			System.out.println(root.left.getValue() + " " + root.right.getValue());
		}
		
		public void Display(){
			treeNode tmp = this.root;
			
			System.out.println(root.getValue());
			System.out.println(root.left.getValue() + " " + root.right.getValue());
		}

}			