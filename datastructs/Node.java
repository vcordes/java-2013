class Node{

		public int value;
		public Node next;
		public Node prev;
	
		public Node(){
			int value = 0;
			next = null;
			prev = null;
		}
		
		public Node(int val){
			value = val;
			next = null;
			prev = null;
		}
		
		public void printValue(){
			System.out.print(value + " ");
		}
		
		public void setValue(int val){
			this.value = val;
		}
		
}
