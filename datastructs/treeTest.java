import java.io.*;
import java.util.*;

class treeTest {
    public static void main(String[] args) {

	    Tree myTree = new Tree();

		Scanner input = new Scanner(System.in);
		int selection = 0;
		String loop = "n";
		int in = 0;

		while (loop != "y"){

			System.out.println("");
			System.out.println("Enter 1 to quit");
			System.out.println("Enter 2 to build a tree");
			System.out.println("Enter 3 for a test");
			System.out.println("Enter 4 to display the tree");

			selection = input.nextInt();

			switch (selection) {
				case 1: loop = "y";
						break;
				case 2: myTree.Build();
						break;
				case 3: myTree.Test();
						break;
				case 4: myTree.Display();
						break;
				default: System.out.println("Not a valid option");
						break;
			}
		}


	}
}