class treeNode{

		private int value;
		public treeNode left;
		public treeNode right;
		public treeNode parent;
	
		public treeNode(){
			int value = 0;
			left = null;
			right = null;
			parent = null;
		}
		
		public treeNode(int val){
			value = val;
			left = null;
			right = null;
		}
		
		public void printValue(){
			System.out.print(value + " ");
		}
		
		public void setValue(int val){
			this.value = val;
		}
		
		public int getValue(){
			return this.value;
		}
		
}
