import java.io.*;
import java.util.*;

class stackTest {
    public static void main(String[] args) {
	
	    Stack myStack = new Stack();
		Node tmp = new Node();
		
		Scanner input = new Scanner(System.in);
		int selection = 0;
		String loop = "n";
		int in = 0;
		
		tmp.setValue(4);
		
		while (loop != "y"){
			
			System.out.println("");
			System.out.println("Enter 1 to quit");
			System.out.println("Enter 2 to push");
			System.out.println("Enter 3 to pop");
			System.out.println("Enter 4 to peek");
			
			
			selection = input.nextInt();
		
			switch (selection) {
				case 1: loop = "y";
						break;
				case 2: myStack.Push(tmp);
						break;
				case 3: myStack.Pop();
						break;
				case 4: myStack.Peek();
						break;
				default: System.out.println("Not a valid option");
						break;
			}
		}
		
		
	}
}