import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.Font;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Dimension;
import java.awt.Graphics;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class intro implements ActionListener
{

	String character = "";
	JLabel prompt;
	JButton shadow;
	JButton light;
	JFrame frame;
	
	public intro ( String character )
			{
			frame = new JFrame( "CASTOR" );			
			frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
			frame.setSize( 500, 300 );

			try {
    		frame.setContentPane(new JLabel(new ImageIcon(ImageIO.read(new File("introbackground.jpg")))));
			} catch (IOException e) {
				e.printStackTrace();
			}
			frame.setVisible( true );

			
			prompt = new JLabel( "hello " + character + " What school would you like to begin your studies in?" );
			frame.getContentPane().add( prompt );	
            prompt.setFont( new Font( "Ariel", Font.BOLD, 20) );
            prompt.setForeground( Color.BLACK);
			
			light = new JButton("Light");
            frame.getContentPane().add(light);
			light.setBackground(Color.YELLOW);
			light.setForeground(Color.ORANGE);
            light.addActionListener(this);
			
			shadow = new JButton("Shadow");
            frame.getContentPane().add(shadow);
			shadow.setBackground(Color.BLACK);
			shadow.setForeground(Color.GRAY);
            shadow.addActionListener(this);
			
			frame.pack();				
            frame.getContentPane( ).setLayout( new FlowLayout());
			}
			
			
	public void actionPerformed (ActionEvent choice)
			{
			try {
			player player1 = new player(character);
			} catch (Exception e) {}

			
			if(choice.getSource() == light)
			{
				try {
					frame.setContentPane(new JLabel(new ImageIcon(ImageIO.read(new File("lightbg.jpg")))));
					} catch (IOException e) {
				e.printStackTrace();
				}
				
			JLabel message = new JLabel("test");
            message.setFont( new Font( "Ariel", Font.BOLD, 20) );
            message.setForeground( Color.YELLOW);
			
			frame.getContentPane().add(message);
			frame.pack();
			frame.getContentPane().setLayout( new FlowLayout());
			}
			else if(choice.getSource() == shadow)
			{
				try {
					frame.setContentPane(new JLabel(new ImageIcon(ImageIO.read(new File("shadowbg.jpg")))));
					} catch (IOException e) {
				e.printStackTrace();

			frame.pack();
			frame.getContentPane().setLayout(new FlowLayout());
				}			
			}			
	}
}