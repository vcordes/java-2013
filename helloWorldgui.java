//just to make sure I have the hello world progs in my repo

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class helloWorldgui
{
	public static void main ( String [] args )
			{
			JFrame frame = new JFrame( "HelloJava3" );
			JLabel label = new JLabel( "Hello World" );
			frame.getContentPane().add( label );
			frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
			frame.setSize( 300, 300 );
			frame.setVisible( true );
			}
}