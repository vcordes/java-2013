import javax.swing.*;
import java.awt.Font;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Dimension;
import java.awt.Graphics;

public class numguess{

        public static void main(String[] args){

                javax.swing.SwingUtilities.invokeLater(new Runnable(){
                        public void run(){
                                Guess guess = new Guess();
                        }
                });
        }
}

class Guess implements ActionListener{
        JFrame frame;
        JLabel label;
        JButton button0;
        JButton button1;
        JButton button2;
        JButton button3;
        JButton button4;
        JButton button5;
        JButton button6;
        JButton button7;
        JButton button8;
        JButton button9;

        public Guess(){
                frame = new JFrame("Number Guessing Game");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                label = new JLabel("Click a button to guess a number from 0 - 9");
                frame.getContentPane().add(label);
                label.setFont(new Font("Courier", Font.BOLD, 20));
                label.setOpaque(true);
                label.setForeground(Color.BLUE);
                
                button0 = new JButton("0");
                frame.getContentPane().add(button0);
                button1 = new JButton("1");
                frame.getContentPane().add(button1);
                button2 = new JButton("2");
                frame.getContentPane().add(button2);
                button3 = new JButton("3");
                frame.getContentPane().add(button3);
                button4 = new JButton("4");
                frame.getContentPane().add(button4);
                button5 = new JButton("5");
                button5.addActionListener(this);
                frame.getContentPane().add(button5);
                button6 = new JButton("6");
                frame.getContentPane().add(button6);
                button7 = new JButton("7");
                frame.getContentPane().add(button7);
                button8 = new JButton("8");
                frame.getContentPane().add(button8);
                button9 = new JButton("9");             
                frame.getContentPane().add(button9);

                Pane pane = new Pane();
                frame.getContentPane().add(pane);
                frame.pack();
                frame.setVisible(true);                                 
                frame.getContentPane().setLayout(new FlowLayout());
        }
        
        public void actionPerformed( ActionEvent evObj){
                label.setText("Correct!");
        }
}

class Pane extends JPanel{
        Dimension dim = new Dimension();
        
        public Pane(){
                dim.setSize(550, 100);
                this.setPreferredSize(dim);
        }
}
