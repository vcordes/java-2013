//Vincent Cordes
//Ethiopian multiplication
//didn't fully test the limit but...
//it breaks at the system "int" size limit

import java.io.*;
import java.util.*;


class ethiMulti{
	public static void main(String[] args){

	//variables to do math on
	int[] left;
	int[] right;
	int i = 0;
	int k = 0;
	left = new int[25];
	right = new int[25];
	
	//scanner objects....cause you need um
	Scanner num1 = new Scanner(System.in);
	Scanner num2 = new Scanner(System.in);
	
	//prompt input
	System.out.println("Enter your first multiplicand");
	left[0] = num1.nextInt();
	
	System.out.println("Enter your second multiplicand");
	right[0] = num2.nextInt();
	
	
	//set up for printing columns
	System.out.println("Left" + "    " + "Right");
	System.out.println(left[0] + "       " + right[0]);
	
	//loop to calculate the values of the columns
	while(left[i] > 1)
	{
		i = i + 1;
		left[i] = (left[i - 1] / 2);
		right[i] = (right[i -1] *2);
		System.out.println(left[i] + "       " + right[i]);	
	}
		
	i = 0;
	
	//break in the space
	System.out.println("");
	System.out.println("Heres the Magic");
	System.out.println("Left" + "    " + "Right");
	System.out.println("");	
	
	//friendly loop to print what columns are being used...
	//and do some math
	while(left[i] > 0)
	{
		if (left[i] % 2 != 0)
		{
			k = k + right[i];
			System.out.println(left[i] + "       " + right[i]);
		}	
	i = i + 1;
	}
	
	
	//prints the answer
	System.out.println("Your answer is: " + k);
			
	}
}