import javax.swing.*;        
import java.awt.Font;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Dimension;
import java.awt.Image;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

public class Demo2 {

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
        Gui gui = new Gui( );
		testGui test = new testGui();
		Gui nextTest = new Gui();
		testGui nextNextTest = new testGui();
            }
        });
    }
}

class Gui implements ActionListener {
    JLabel label;

    public Gui( ) {
        JFrame frame = new JFrame("Hello");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout( new FlowLayout( ) );
        label = new JLabel("Hello");
        frame.getContentPane().add(label);

        label.setFont( new Font( "Courier", Font.BOLD, 18 ) );
        label.setBackground( Color.BLUE );
        label.setOpaque( true );
        label.setForeground( Color.YELLOW );

        JButton button = new JButton( "Click 1" );
        frame.getContentPane( ).add( button );
        button.addActionListener( this );

        JButton button2 = new JButton( "Click 2" );
        frame.getContentPane( ).add( button2 );
        button2.addActionListener( this );

        Pan pan = new Pan( );
        frame.getContentPane( ).add( pan );

        frame.pack();
        frame.setVisible(true);
    }

    public void actionPerformed( ActionEvent evObj ) {
//        label.setText( "been clicked" );
        if( ( (JButton)evObj.getSource( ) ).getText(  ) == "Click 1" ) label.setText( "1 clicked" );
        if( ( (JButton)evObj.getSource( ) ).getText(  ) == "Click 2" ) label.setText( "2 clicked" );
    }

}

class Pan extends JPanel {
    Image img;

    public Pan( ) {
        this.setPreferredSize( new Dimension( 100, 100 ) );
        try {
            img = ImageIO.read( new File( "back.png" ) );
        }
        catch( IOException ioex ) {
            System.out.println( ioex.toString( ) );
        }
    }

    public void paintComponent (Graphics g) {
        int spacing = 10;
        super.paintComponent( g );
        g.drawImage( img, 0, 0, null );
        Graphics2D g2 = (Graphics2D) g;
        for( int i = 0; i < 3; i++ ) {
            g2.drawLine( 10, 10 + i * spacing, this.getWidth( ) - 10, 10 + i * spacing );
        }
    }    
}

class testGui implements ActionListener {
    JLabel label;

    public testGui( ) {
        JFrame frame = new JFrame("test");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout( new FlowLayout( ) );
        label = new JLabel("Heyo!!");
        frame.getContentPane().add(label);

        label.setFont( new Font( "Ariel", Font.BOLD, 24 ) );
        label.setBackground( Color.MAGENTA );
        label.setOpaque( true );
        label.setForeground( Color.RED );

        JButton button = new JButton( "Click 3" );
        frame.getContentPane( ).add( button );
        button.addActionListener( this );

        JButton button2 = new JButton( "Click 4" );
        frame.getContentPane( ).add( button2 );
        button2.addActionListener( this );

        Pan pan = new Pan( );
        frame.getContentPane( ).add( pan );

        frame.pack();
        frame.setVisible(true);
    }

    public void actionPerformed( ActionEvent evObj ) {
//        label.setText( "been clicked" );
        if( ( (JButton)evObj.getSource( ) ).getText(  ) == "Click 3" ) label.setText( "3 clicked" );
        if( ( (JButton)evObj.getSource( ) ).getText(  ) == "Click 4" ) label.setText( "4 clicked" );
    }

}